
" Fundo transparente...
hi Normal guibg=NONE ctermbg=NONE

" Linha do cursor...
hi CursorLine guibg=#202130

" Linha de coluna...
hi ColorColumn guibg=#202130

" Comentários em itálico...
hi Comment cterm=italic gui=italic

" Divisão vertical de janelas...
hi VertSplit ctermbg=NONE guibg=NONE ctermfg=7 guifg=#2c324d

" Barra de abas...
hi TabLine      guifg=#9192a0 guibg=#2c324d gui=none
hi TabLineSel   guifg=#a1a2b0 guibg=NONE gui=bold
hi TabLineFill  guifg=#9192a0 guibg=#2c324d gui=none

" Seleção (modo visual)...
hi Visual guifg=NONE guibg=#303140

" Cores das dobras (folding)...

hi Folded guibg=NONE guifg=#505160 gui=italic cterm=italic
